% Project 4 writeup

\documentclass[11pt]{article}

\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}

\usepackage{hyperref}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\usepackage{fontspec}
\setmainfont{Liberation Serif}

\usepackage{polyglossia}
\setmainlanguage{english}

\usepackage{geometry}
\geometry{letterpaper, total={6.5in,9in}}
 
\usepackage{graphicx}
\usepackage{subcaption}
\graphicspath{ {./images/} }

\DeclareMathOperator{\erf}{erf}
\DeclareMathOperator{\erfc}{erfc}
\DeclareMathOperator{\Q}{Q}
\DeclareMathOperator{\E}{\mathbb{E}}
\DeclareMathOperator{\var}{var}

\newcommand{\reffig}[1]{Figure~\ref{#1}}
\newcommand{\reftab}[1]{Table~\ref{#1}}

\begin{document}

\begin{flushleft}
CMPE320 Probability, Statistics, and Random Processes \\
Kira Singla (They/Them) \\
ksingla1@umbc.edu \\
\end{flushleft}

\section*{Project 4}

\subsection*{Introduction}
\quad This project asks us to use Maximum A Posteriori (MAP) and Maximum Likelyhood (ML) estimators on a Binary Amplitude Shift Keyed (BASK) signal.

\subsection*{Problem 2.1}

\quad The hypotheses for this MAP problem are trivial, \(H_0\) is the event in which the transmitted message was \(+A\), and \(H_1\) is the event in which the transmitted message was \(-A\).
We know that \(R = M + N\), and with \(M\) fixed this makes \(R\) a Gaussian with the same variance as \(N\) and a mean of \(M\).
Then the conditional PDF for \(H_0\) is
\[
f_{R \vert H_0} (r \vert H_0) = f_{R \vert A} (r \vert +A) = f_N(r-A)
\]
and the conditional PDF for \(H_1\) is
\[
f_{R \vert H_1} (r \vert H_1) = f_{R \vert -A} (r \vert -A) = f_N(r+A)
\]
The conditional PDF for \(H_0\) can be transformed into a joint PDF in the following procedure,
\begin{align*}
f_{RM} (r,A) &= f_M(A) f_{R \vert A} (r \vert A) \\ 
             &= p_0 f_N(r-A)
\end{align*}
and in the same manner, the conditional PDF for \(H_1\) can be transformed, 
\begin{align*}
f_{RM} (r,-A) &= f_M(-A) f_{R \vert -A} (r \vert -A) \\ 
             &= (1-p_0) f_N(r+A)
\end{align*}

The events conditioned on a \(B=0\) or \(B=1\) are identical to \(H_0\) and \(H_1\) respectively due to the 1-to-1 correspondence between \(B\) and \(M\), so the following statements are true:
\begin{gather*}
f_{RM}(r,m \vert b = 0) = f_{RM}(r,A) \\
f_{RM}(r,m \vert b = 1) = f_{RM}(r,-A)
\end{gather*}

The Likelyhood Ratio \(L\) is thus
\[
L = \frac{ f_{RM}(r,A) }{ f_{RM}(r,-A) }
\]
Our goal is to find the Log Likelyhood Ratio \(\ln(L) = \Lambda \), and we take advantage of logarithm rules to simplify this.
\begin{align*}
\Lambda &= \ln\left(\frac{ f_{RM}(r,A) }{ f_{RM}(r,-A) }\right) \\
        &= \ln(f_{RM}(r,A)) - \ln(f_{RM}(r,-A)) \\
        &= \ln(p_0 f_N(r-A)) - \ln((1-p_0) f_N(r+A)) \\
        &= \ln\left(\frac{p_0}{1-p_0}\right) + \ln(f_N(r-A)) - \ln(f_N(r+A)) \\
        &= \ln\left(\frac{p_0}{1-p_0}\right) - \frac{(r-A)^2}{2 \sigma^2} + \frac{(r+A)^2}{2 \sigma^2}
\end{align*}

We then solve \(\Lambda = 0\) for \(r\), whose value will be called \(\tau_{MAP}\).
\begin{gather*}
\Lambda = 0 \\
\ln\left(\frac{p_0}{1-p_0}\right) - \frac{(r-A)^2}{2 \sigma^2} + \frac{(r+A)^2}{2 \sigma^2} = 0 \\
- \frac{(r-A)^2}{2 \sigma^2} + \frac{(r+A)^2}{2 \sigma^2} = - \ln\left(\frac{p_0}{1-p_0}\right) \\
-(r-A)^2 + (r+A)^2 = -2 \sigma^2 \ln\left(\frac{p_0}{1-p_0}\right) \\
4rA = -2 \sigma^2 \ln\left(\frac{p_0}{1-p_0}\right) \\
r = -\frac{ \sigma^2 }{2A} \ln\left(\frac{p_0}{1-p_0}\right) \\
\tau_{MAP} =  -\frac{ \sigma^2 }{2A} \ln\left(\frac{p_0}{1-p_0}\right) 
\end{gather*}

Given the structure of our MAP detector, we will hypothesize that a recieved signal \(r_k\) corresponds to an original signal \(\hat{b_k}\) according to the following rule
\[
\hat{b}_k = 
    \begin{cases}
    1,\, r_k < \tau_{MAP} \\
    0,\, r_k \ge \tau_{MAP}
    \end{cases}
\]


This value of \(\tau_{MAP}\) depends explicitly on \(p_0\) via the term \(-\ln\left(\frac{p_0}{1-p_0}\right)\).
This term has a positive value for \(p_0 < 0.5\), increasing asymptotically as \(p_0 \rightarrow 0\).
The term is 0 for \(p_0 = 0.5\).
This term has a negative value for \(p_0 > 0.5\), decreasing asymptotically as \(p_0 \rightarrow 1\).

Given \(\gamma_{dB} = 10 \text{ dB}\) and the equation \( \gamma_{dB} = -10 \log_{10}(\frac{\sigma^2}{A^2})\),
we conclude \(\sigma^2 = \frac{A^2}{10}\).
For \(A=3\), this gives us a function \(\tau_{MAP}(p_0) = -\frac{3}{20} \ln(\frac{p_0}{1-p_0})\).
This function is graphed on \( [0.01, 0.99] \) in \reffig{fig:p1}.
This curve is readily explained by observing that for low \(p_0\), the majority of signals are expected to be originally negative,
so we expect that for values approaching zero,
the originally negative signals will be making a larger contribution than the originally positive signals,
so a signal observed in the range of values near zero is more likely to have originated from a transmitted negative signal.
Thus we set the threshold higher than zero for low \(p_0\), so that most of the signals near zero will be predicted to be from a negative signal.
The converse is true for high \(p_0\) and transmitted positive signals, so the threshold becomes negative.

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.5\linewidth]{problem-2-1-fig-1.png}
\caption{}
\label{fig:p1}
\end{figure}

\newpage
\subsection*{Problem 2.2}

\qquad As was previously stated but will be demonstrated here in more detail, \(\gamma_{dB} = 10 \Rightarrow \sigma^2 = \frac{A^2}{10}\), and for the given value of A, \(\sigma^2 = 0.9\).
\begin{gather*}
\gamma_{dB} = -10 \log_{10}\left(\frac{\sigma^2}{A^2}\right) \\
-\frac{\gamma_{dB}}{10} = log_{10}\left(\frac{\sigma^2}{A^2}\right) \\
10^{-\frac{\gamma_{dB}}{10}} = \frac{\sigma^2}{A^2} \\
\sigma^2 = A^2 10^{-\frac{\gamma_{dB}}{10}} \\
\sigma^2 = \frac{A^2}{10} \\
\sigma^2 = 0.9
\end{gather*}

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.5\linewidth]{problem-2-2-fig-1.png}
\caption{}
\label{fig:p2}
\end{figure}

\reffig{fig:p2}, shows how the data will be interpreted for \(p_0=0.25\).
The hump to the left of the \(\tau_{MAP}\) divider is approximately three times as tall as the right hump.
This hump should contain three times as many data points as the right hump because \( p_1 = 3p_0 \text{ for } p_0=0.25 \),
and this is what the relative height differences represent in the underlying data.

\newpage
\subsection*{Problem 2.3}

\quad If we assume that \(p_0 = p_1\), then we conclude \(p_0 = 0.5\), so \(\tau_{ML}(0.5) = -\frac{3}{20} \ln(1) = 0\). 
When 0 and 1 are both equally likely signals, the transmitted data should be entirely symmetric about the midpoint (0 Volts in this case), so the detection threshold should be at this midpoint as well.
The logical and mathematical arguments both agree that \(\tau_{ML} = 0\) makes sense. 

We recall from Problem 2.1 that the conditional PDF for \(H_0\) is 
\[
f_{R \vert H_0} (r \vert H_0) = \frac{1}{\sqrt{2\pi \sigma^2}} \exp\left(-\frac{(r-A)^2}{2\sigma^2}\right)
\]
and we know that the region over which the ML detector will select a 1 is \( (-\infty, 0) \), so we have the integral
\[
p_{f1} = \int_{-\infty}^{0} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp\left(-\frac{(r-A)^2}{2\sigma^2}\right)dr
\]
We solve this integral in terms of \texttt{erfc()} because Julia does not have any \textrm{Q()} function calls.
\begin{gather*}
p_{f1} = \int_{-\infty}^{0} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp\left(-\frac{(r-A)^2}{2\sigma^2}\right)dr \\
\text{Let } u = \frac{r-A}{\sigma} \Rightarrow \frac{dr}{\sigma} = du \\
r_0 = -\infty \Rightarrow u_0 = -\infty \\
r_1 = 0 \Rightarrow u_1 = -\frac{A}{\sigma} \\
p_{f1} = \frac{1}{\sqrt{2\pi}} \int_{-\infty}^{-\frac{A}{\sigma}} \exp\left( -\frac{u^2}{2}\right)du \\
p_{f1} = \Q\left(\frac{A}{\sigma}\right) \\
p_{f1} = 0.5\erfc \left( \frac{A}{\sqrt{2\sigma^2}} \right)
\end{gather*}

We recall from Problem 2.1 that the conditional PDF for \(H_1\) is
\[
f_{R \vert H_1} (r \vert H_1) = \frac{1}{\sqrt{2\pi \sigma^2}} \exp \left( -\frac{(r+A)^2}{2\sigma^2} \right)
\]
and we know that the region over which the ML detector will select a 1 is \( [0, \infty) \), so we have the integral
\[
p_{f0} = \int_{0}^{\infty} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp \left(-\frac{(r+A)^2}{2\sigma^2}\right) dr
\]
We solve this integral in terms of \texttt{erfc()} because Julia does not have any \textrm{Q()} function.
\begin{gather*}
p_{f0} = \int_{0}^{\infty} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp\left(-\frac{(r+A)^2}{2\sigma^2}\right)dr \\
\text{Let } u = \frac{r+A}{\sigma} \Rightarrow \frac{dr}{\sigma} = du \\
r_0 = 0 \Rightarrow u_0 = \frac{A}{\sigma} \\
r_1 = \infty \Rightarrow u_1 = \infty \\
p_{f0} = \frac{1}{\sqrt{2\pi}} \int_{\frac{A}{\sigma}}^{\infty} \exp \left( -\frac{u^2}{2} \right)du \\
p_{f0} = \Q \left( \frac{A}{\sigma} \right) \\
p_{f0} = 0.5 \erfc \left( \frac{A}{\sqrt{2\sigma^2}} \right)
\end{gather*}

We use the principle of total probability to assert that \(p_{BT} = p_1p_{f0} + p_0p_{f1}\).
\begin{gather*}
p_{BT} = p_1p_{f0} + p_0p_{f1} \\
p_{BT} = 0.5p_{f0} + 0.5p_{f1} \\
p_{BT} = 0.5\erfc \left( \frac{A}{\sqrt{2\sigma^2}} \right)
\end{gather*}
For the earlier given value of \(\gamma_{dB} = 10,\, p_{BT} \approx 0.00078\).

To begin our simulation, we use the earlier generalized expression for converting between \(\gamma_{dB}\) and \(\sigma^2\) to convert the array of SNRs into variances.
We run a simulation for each of these variances, compute the relative error rate, and graph those alongside the theoretical rates computed from the earlier equations.
For every simulation, we use \(n = 10^7\).
This graph is shown in \reffig{fig:p3}, and shows that the measured error rates diverge slightly (and noisily) from the theoretical rates as \(\gamma_{dB} \rightarrow 14\).
For \(\gamma_{dB} \le 12\), the graphs overlap almost completely.
The underlying reason for this divergence is that the error rates approach \(10^{-7}\) in that neighborhood, so there are not enough trials for the WLLN to give us a better theoretical result.
If we used \(n = 10^8\) there is a good chance this would not be the case, however the computational time would expand in polynomial time.
Memory limits are thankfully not an issue at this scale, but it is still not viable for experimental use.

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.5\linewidth]{problem-2-3-fig-1.png}
\caption{}
\label{fig:p3}
\end{figure}

\newpage
\subsection*{Problem 2.4}

\quad We repeat the derivations for \(p_{f1}\) and \(p_{f0}\) in the MAP case.
The MAP detector will select a 1 if the recieved value is in \( (-\infty, \tau_{MAP}\), and the conditional PDF for \(H_0\) is the same as in the last problem, so we have the following integral:
\[
p_{f1} = \int_{-\infty}^{\tau_{MAP}} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp \left( -\frac{(r-A)^2}{2 \sigma^2} \right) dr
\]
Following the same procedure as in Problem 2.3, we solve this integral:
\begin{gather*}
p_{f1} = \int_{-\infty}^{\tau_{MAP}} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp \left( -\frac{(r-A)^2}{2 \sigma^2} \right) dr \\
\text{Let } u = \frac{r-A}{\sigma} \Rightarrow \frac{dr}{\sigma} = du \\
r_0 = -\infty \Rightarrow u_0 = -\infty \\
r_1 = -\frac{\sigma^2}{2A} \ln \left( \frac{p_0}{1-p_0} \right) \Rightarrow u_1 = -\frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) - \frac{A}{\sigma} \\
p_{f1} = \int_{-\infty}^{u_1} \frac{1}{\sqrt{2\pi}} \exp \left( -\frac{u^2}{2} \right) du \\
p_{f1} = \int_{-u_1}^{\infty} \frac{1}{\sqrt{2\pi}} \exp \left( -\frac{u^2}{2} \right) du \\
p_{f1} = \Q \left( \frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) + \frac{A}{\sigma} \right) \\
p_{f1} = 0.5 \erfc \left( \frac{1}{\sqrt{2}} \left( \frac{A}{\sigma} + \frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) \right) \right) \\
\end{gather*}

The MAP detector will select a 0 if the recieved value is in \( [\tau_{MAP}, \infty) \), and the conditional PDF for \(H_1\) is the same as in the last problem, so we have the following integral:
\[
p_{f0} = \int_{\tau_{MAP}}^{\infty} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp \left( -\frac{(r+A)^2}{2 \sigma^2} \right) dr
\]
Following the same procedure as in Problem 2.3, we solve this integral:
\begin{gather*}
p_{f0} = \int_{\tau_{MAP}}^{\infty} \frac{1}{\sqrt{2 \pi \sigma^2}} \exp \left( -\frac{(r+A)^2}{2 \sigma^2} \right) dr \\
\text{Let } u = \frac{r+A}{\sigma} \Rightarrow \frac{dr}{\sigma} = du \\
r_0 = -\frac{\sigma^2}{2A} \ln \left( \frac{p_0}{1-p_0} \right) \Rightarrow u_0 = -\frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) + \frac{A}{\sigma} \\
r_1 = \infty \Rightarrow u_1 = \infty \\
p_{f0} = \int_{u_0}^{\infty} \frac{1}{\sqrt{2\pi}} \exp \left( -\frac{u^2}{2} \right) du \\
p_{f0} = \Q \left( \frac{A}{\sigma} -\frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) \right) \\
p_{f0} = 0.5 \erfc \left( \frac{1}{\sqrt{2}} ( \frac{A}{\sigma} -\frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) ) \right) \\
\end{gather*}

We use the principle of total probability to assert that \(p_{BT} = p_1p_{f0} + p_0p_{f1}\) as in the previous problem.
\begin{gather*}
p_{BT} = p_1p_{f0} + p_0p_{f1} \\
p_{BT} = 0.75p_{f0} + 0.25p_{f1} \\
p_{BT} = 0.375 \erfc \left( \frac{1}{\sqrt{2}} \left( \frac{A}{\sigma} - \frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) \right) \right) 
       + 0.125 \erfc \left( \frac{1}{\sqrt{2}} \left( \frac{A}{\sigma} + \frac{\sigma}{2A} \ln \left( \frac{p_0}{1-p_0} \right) \right) \right)
\end{gather*}
For the earlier given value of \(\gamma_{dB} = 10,\, p_{BT} \approx 0.00067\).


\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.5\linewidth]{problem-2-4-fig-1.png}
\caption{}
\label{fig:p4}
\end{figure}

The simulation was carried out in an almost identical procedure as the previous, and the results in \reffig{fig:p4} have roughly similar patterns with both curves being smooth until a certain point.
This roughness starts at a similar point, and can be attributed to the same relationship with the WLLN.
The graphs have a unique divergence wherein, though both smooth, \(p_{BX}\) has a higher value than \(p_{BT}\) for low values of \(\gamma_{dB}\). 


\newpage
\subsection*{Problem 2.5}

\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.5\linewidth]{problem-2-5-fig-1.png}
\caption{}
\label{fig:p5a}
\end{figure}

\qquad Both plots in \reffig{fig:p5a} have similar shapes, being downward concave curves with similar values.
The only substantial difference between them is that the ML error rate is slightly higher for all values of \(\gamma_{dB}\). 


\begin{figure}
\centering
\captionsetup{justification=centering}
\includegraphics[width=0.5\linewidth]{problem-2-5-fig-2.png}
\caption{}
\label{fig:p5b}
\end{figure}

This ratio hangs around 0.85, which means that MAP has a lower probability of error than ML for all graphed values of \(\gamma_{dB}\).
This is expected given the graphs in \reffig{fig:p5a} show the error rate is consistently higher for ML than MAP.
This ratio is roughly what would be expected given the situation where \(p_0\) is near 0.5 will have less differentiation (in the sense of Shannon's information content) between the two possible outcomes, and the situation where \(p_0\) is not near 0.5 will have more differentiation between the possible outcomes.
Applying this difference in the possible information content of the transmissions to the MAP cases mean that it should be easier to differentiate the signals for \(p_0 = 0.25\) than for \(p_0 = 0.5\), which means that MAP should have a lower error rate than ML. 

\newpage
\subsection*{What I Learned}

\qquad One of the single most important things this project taught me, or reminded me of, is that a mathematical computation with a goal will always be more compelling than one without a goal.
This project showed me just how much I wanted one of these class projects to have a concrete conclusion that I could make about a real world process, as opposed to very theoretical and abstracted problems that are often unmotivated.
Though we still do not have much application for this signal, there is a very concrete and \textit{advanced} use case for this signal processing and that makes it more interesting.

This project taught me repeatedly just how important it is to be careful when reading documentation, because there were several unique instances in which I had to dedicate a lot of time to debugging code whose only error was making incorrect calls to the various libraries I was using.
These were easily remedied once I figured out the problem, but they were frustrating to debug.
There is good reason to think that some of this is just because I have not been programming that often in this semester, only working on assignments for this specific class.
I made full use of git during this project, making it the largest git codebase that I have built from the ground up and regularly committed to.
This project was more complicated than the previous ones, but I still found it very straightforward to implement once I spent enough time to fully understand what the project was asking of us, and I would estimate that it took me around 10 hours.
I think this project could have been much harder, especially given how important this is supposed to be in the scope of the class.
Something that I saw very consistently from students in this class is that they had trouble understanding the project due to the many typos in the project specification, and I am hopeful that if the project specification was rewritten then students would have an easier time with this project and it would be able to be expanded to teach them more about MAP and MLE algorithms. 
Using several higher level calls including the built in matplotlib histogram binning calls removed a layer of complexity that I had to think about, and this vastly simplified the code I wrote.
This allowed me to focus less on organizing my code and more on the abstract design, which was helpful given the increased complexity of this project's logic.

\end{document}
