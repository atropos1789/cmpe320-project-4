#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
Reference non-vectorized implementation to find the error rate of the problem 3 MLE 
=#

using Random
using Distributions
using SpecialFunctions


## Problem Parameters

A::Float64 = 3.0
# γ_dB::Float64 = 12.6 
# just code the explicit value from the known formula
σ2::Float64 = 0.4945867864718621
p_0::Float64 = 0.5
τ_ML::Float64 = 0.0
n::Int64 = 10^7
Bdist = Distributions.Bernoulli(1.0 - p_0) 
Ndist = Distributions.Normal(0, sqrt(σ2))


## Generate Samples from B, M, N, and R

B = Random.rand(Bdist, n)

M = zeros(n)
for i in 1:n
    if B[i] == 1
        M[i] = -1.0*A
    elseif B[i] == 0
        M[i] = A
    else 
        println("WARNING: B has unexpected value")
    end
end

N = Random.rand(Ndist, n)

R = M + N


## Apply MLE to the Recieved Signal to Generate Bhat

Bhat = zeros(n)
for i in 1:n
    if R[i] < τ_ML
        Bhat[i] = 1
    else
        Bhat[i] = 0
    end
end


## Compute the number of errors

errors = sum(abs.(B - Bhat))
theoretical_errrors = 0.5*erfc(3/sqrt(2*σ2))

println("Number of Errors: ", errors)
println("Error Rate: ", errors/n)
println("Theoretical Error Rate: ", theoretical_errrors)
