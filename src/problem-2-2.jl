#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
The graph for problem 2 of project 4
=#

using JLD2
using Random
using Statistics
using Distributions
using PyPlot


## Problem Parameters
A::Float64 = 3.0
σ2::Float64 = 0.9 
p_0::Float64 = 0.25
τ_MAP::Float64 = -0.15*log( (p_0)/(1-p_0) )
n::Int64 = 100000
# Distributions.jl's implementation of Bernoulli() is parameterized by p_1 
Bdist = Distributions.Bernoulli(1.0 - p_0) 
Ndist = Distributions.Normal(0, sqrt(σ2))

function fR(x, p_0, A, σ2)::Float64
    (1.0 - p_0) * Distributions.pdf(Distributions.Normal(-1*A, sqrt(σ2)), x) + p_0 * Distributions.pdf(Distributions.Normal(A, sqrt(σ2)), x)
end


## Generate Samples from B, M, N, and R

B = zeros(n)
B = Random.rand(Bdist, n)

M = zeros(n)
for i in 1:n
    if B[i] == 1
        M[i] = -1.0*A
    elseif B[i] == 0
        M[i] = A
    end
end

N = zeros(n)
N = Random.rand(Ndist, n)

R = M + N


## Apply MAP to the Recieved Signal to Generate Bhat

Bhat = zeros(n)
for i in 1:n
    if R[i] < τ_MAP
        Bhat[i] = 1
    else
        Bhat[i] = 0
    end
end


## Graph Data from R, the analytical PDF, and τ_MAP

figure(2)
axis([-7.0, 7.0, 0.0, 0.4])

histbins = [ 0.25*i - 7.0 for i=0:56 ]
hist(R, density=true, bins=histbins, color="tab:blue", label="data")

vlines(τ_MAP, ymin=0, ymax=0.5, linestyles="dashed", color="tab:red", label=raw"$τ_{MAP}$")

x = range(start=-7, step=0.01, stop=7)
plot(x, fR.(x, p_0, A, σ2), color="tab:orange", label=raw"$f_R(r)$")

xlabel(raw"$r$")
ylabel(raw"Probability / Probability Density")
title(raw"Recieved Data for $p_0=0.25$, $\tau_{MAP}$, and $f_R(r)$")
figlegend()
savefig("../images/problem-2-2-fig-1.png", dpi=288)


## Save Data to Disk

save_object("../data/M_MAP.jld2", M)
save_object("../data/R_MAP.jld2", R)
save_object("../data/Bhat_MAP.jld2", Bhat)
