#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
problem 5 graphs
=#

using JLD2
using Random
using Statistics
using Distributions
using SpecialFunctions
using PyPlot

## Functions

function SNR_to_var(γ_dB::Float64, A)::Float64
    A^2 * 10.0^(-1*γ_dB/10.0)
end

function MAP_theoretical_error_rate(A::Float64, γ_dB::Float64, p_0::Float64)::Float64
    σ2 = SNR_to_var(γ_dB, A)
    σ = sqrt(σ2)
    arg1 = 1/sqrt(2) * (A/σ - σ/(2A) * log(p_0/(1-p_0)) )
    arg2 = 1/sqrt(2) * (A/σ + σ/(2A) * log(p_0/(1-p_0)) ) 
    return 0.375*erfc(arg1) + 0.125*erfc(arg2)
end

function ML_theoretical_error_rate(A::Float64, γ_dB::Float64)::Float64
    σ2 = SNR_to_var(γ_dB, A)
    return 0.5*erfc(A/sqrt(2*σ2))
end


## Problem Parameters

A::Float64 = 3.0
γ_dB::Vector{Float64} = vcat([0.5*i + 1 for i=0:18], [0.25*i + 10 for i=1:16])
σ2::Vector{Float64} = SNR_to_var.(γ_dB, A)
num_entries::Int64 = length(σ2)
p_0::Float64 = 0.25
n::Int64 = 10^7
error_rates::Vector{Float64} = zeros(num_entries)



figure(5)

x = range(start=1, step=0.01, stop=14)
semilogy(x, ML_theoretical_error_rate.(A, x), label=raw"ML Error Rate")
semilogy(x, MAP_theoretical_error_rate.(A, x, p_0), label=raw"MAP Error Rate")

xlabel(raw"$\gamma_{dB}$")
ylabel(raw"Relative Error in Recieved Data")
title(raw"ML and MAP Detector Performance")
figlegend()

savefig("../images/problem-2-5-fig-1.png", dpi=288)


figure(6)
plot(x, (MAP_theoretical_error_rate.(A, x, p_0))./(ML_theoretical_error_rate.(A, x)))

xlabel(raw"$\gamma_{dB}$")
ylabel(raw"$\frac{MAP\, p_{BT}}{ML\, p_{BT}}$")
title(raw"Ratio of MAP to ML Error")
savefig("../images/problem-2-5-fig-2.png", dpi=288)
