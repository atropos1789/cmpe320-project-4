#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
The graph for problem 1 of project 4
=#

using PyPlot

## Plotting the Graph

tauMAP(pzero) =  (-3.0/20.0) * log( (pzero)/(1-pzero))
pzero = range(0.01, step=0.01, stop=0.99)

figure(1)
xlabel(raw"$p_0$")
ylabel(raw"$\tau_{MAP}$")
title(raw"MAP Threshold Values for Different $p_0$")
plot(pzero, tauMAP.(pzero), label=raw"$\tau_{MAP}$")
figlegend()
savefig("../images/problem-2-1-fig-1.png", dpi=288)
