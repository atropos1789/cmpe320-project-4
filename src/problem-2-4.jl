#!/usr/bin/env julia
#=
CMPE 320
Kira Singla - ksingla1@umbc.edu
problem 4 code
=#

using JLD2
using Random
using Statistics
using Distributions
using SpecialFunctions
using PyPlot

## Functions

function SNR_to_var(γ_dB::Float64, A)::Float64
    A^2 * 10.0^(-1*γ_dB/10.0)
end

function MAP_theoretical_error_rate(A::Float64, γ_dB::Float64, p_0::Float64)::Float64
    σ2 = SNR_to_var(γ_dB, A)
    σ = sqrt(σ2)
    arg1 = 1/sqrt(2) * (A/σ - σ/(2A) * log(p_0/(1-p_0)) )
    arg2 = 1/sqrt(2) * (A/σ + σ/(2A) * log(p_0/(1-p_0)) ) 
    0.375*erfc(arg1) + 0.125*erfc(arg2)
end


## Problem Parameters

A::Float64 = 3.0
γ_dB::Vector{Float64} = vcat([0.5*i + 1 for i=0:18], [0.25*i + 10 for i=1:16])
σ2::Vector{Float64} = SNR_to_var.(γ_dB, A)
num_entries::Int64 = length(σ2)
p_0::Float64 = 0.25
τ_MAP::Float64 = -0.15*log( (p_0)/(1-p_0) )
n::Int64 = 10^7
Bdist = Distributions.Bernoulli(1.0 - p_0) 
Ndists = [ Distributions.Normal(0, sqrt(σ2[i])) for i=1:num_entries ]
error_rates::Vector{Float64} = zeros(num_entries)


## Generate Samples from B, M

B = zeros(n)
B = Random.rand(Bdist, n)

M = zeros(n)
for i in 1:n
    if B[i] == 1
        M[i] = -1.0*A
    elseif B[i] == 0
        M[i] = A
    end
end

for i = 1:num_entries

    ## Generate Samples from N, R
    
    N = zeros(n)
    R = zeros(n)
    N = Random.rand(Ndists[i], n)
    R = M + N

    ## Apply MAP to the Recieved Signal to Generate Bhat

    Bhat = zeros(n)
    for j in 1:n
        if R[j] < τ_MAP
            Bhat[j] = 1
        else
            Bhat[j] = 0
        end
    end
    errors = sum(abs.(B - Bhat))
    theoretical_errrors = MAP_theoretical_error_rate(A, γ_dB[i], p_0)
    global error_rates[i] = errors/n

    println("------------------------------------------------------")
    println("γ_dB = ", γ_dB[i])
    println("σ2 = ", σ2[i])
    println("Number of Errors: ", errors)
    println("Error Rate: ", error_rates[i])
    println("Theoretical Error Rate: ", theoretical_errrors)
    
end

figure(4)
semilogy(γ_dB, error_rates, label=raw"$p_{BX}(\gamma_{dB})$")

x = range(start=1, step=0.01, stop=14)
semilogy(x, MAP_theoretical_error_rate.(A, x, p_0), label=raw"$p_{BT}(\gamma_{dB})$")

xlabel(raw"$\gamma_{dB}$")
ylabel(raw"Relative Error in Recieved Data")
title(raw"$p_{BT}$ versus $p_{BX}$ for different $\gamma_{dB}$ using MAP")
figlegend()

savefig("../images/problem-2-4-fig-1.png", dpi=288)


## Save Data to Disk

save_object("../data/gamma_dB_values_p4.jld2", γ_dB)
save_object("../data/error_rates_p4.jld2", error_rates)
